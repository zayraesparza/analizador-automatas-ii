package EstructuraLista;

public class ListaDinamica<T> {

    private Nodo<T> primero;
    private Nodo<T> ultimo;
    private int tamaño;
    int contador = 0;

    public ListaDinamica() {
        primero = null;
        ultimo = null;
        tamaño = 0;
    }

    public boolean isEmpty() {
        return tamaño == 0;
    }

    public int size() {
        return tamaño;
    }

    public T getDato(int index) {

        if (isEmpty() || (index < 0 || index >= size())) {
            return null;
        } else if (index == 0) {
            return getFirst();
        } else if (index == size()-1) {
            return getLast();
        } else {
            Nodo<T> buscado = getNode(index);
            return buscado.getElemento();
        }
    }

    public T getFirst() {
        if (isEmpty()) {
            return null;
        } else {
            return primero.getElemento();
        }
    }

    public T getLast() {
        if (isEmpty()) {
            return null;
        } else {
            return ultimo.getElemento();
        }
    }

    private Nodo<T> getNode(int index) {
        Nodo<T> buscado = primero;

        contador = 0;
        while (contador < index) {
            contador++;
            buscado = buscado.getSiguiente();
        }
        return buscado;
    }

    /**
     * Añade un elemento al final de la lista
     *
     * @param elemento
     * @return elemento añadido
     */
    public T add(T elemento) {

        Nodo<T> aux;

        if (isEmpty()) {
            aux = new Nodo<>(elemento, null);
            primero = aux;
            ultimo = aux;

        } else {

            aux = new Nodo<>(elemento, null);
            ultimo.setSiguiente(aux);
            ultimo = aux;

        }
        tamaño++;
        return ultimo.getElemento();

    }
    
    public T modIndex(int index, T elemento) {
        Nodo<T> aux = getNode(index);
        aux.setElemento(elemento);
        return aux.getElemento();
    }

    /**
     * Devuelve el estado de la lista
     *
     * @return
     */
    @Override
    public String toString() {

        String contenido = "";
        if (isEmpty()) {
            contenido = "Lista vacia";
        } else {

            Nodo<T> aux = primero;
            while (aux != null) {
                contenido += aux.toString();
                aux = aux.getSiguiente();
            }
        }
        return contenido;
    }
    
}