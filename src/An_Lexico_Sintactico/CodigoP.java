package An_Lexico_Sintactico;

import An_Lexico_Sintactico.ArbolSintacticoAbstracto;
import EstructuraLista.ListaDinamica;
import EstructuraPila.PilaDinamica;

public class CodigoP {

    static PilaDinamica<Valor> pila = new PilaDinamica<>();
    static PilaDinamica<String> pilaP = new PilaDinamica<>();
    static PilaDinamica<String> pilaP2 = new PilaDinamica<>();
    static ListaDinamica<String> mov = new ListaDinamica<>();
    Valor pop;
    String str, v1, v2;

    CodigoP() {

    }

    public PilaDinamica<Valor> codigo(PilaDinamica<Valor> pila) {

        pop = pila.pop();
        v1 = pop.valor;
        pop = pila.pop();
        v2 = pop.valor;
        pilaP.push("LDCI eax, " + v1);
        pop = pila.pop();
        try {
            Integer.parseInt(v2);
            switch (pop.Nombre) {
                case "-":
                    pilaP.push("RESTI eax, " + v2);
                case "+":
                    pilaP.push("SUMI eax, " + v2);
                case "*":
                    pilaP.push("MULTI eax, " + v2);
            }
        } catch (Exception e) {
            switch (pop.Nombre) {
                case "-":
                    pilaP.push("RESTR eax, " + v2);
                case "+":
                    pilaP.push("SUMR eax, " + v2);
                case "*":
                    pilaP.push("MULTR eax, " + v2);
            }
        }

        while (!pila.isEmpty()) {
            pop = pila.pop();
            if (!pop.Nombre.equals(":=")) {
                v2 = pop.valor;
                pop = pila.pop();
                try {
                    Integer.parseInt(v2);
                    switch (pop.Nombre) {
                        case "-":
                            pilaP.push("RESTI eax, " + v2);
                        case "+":
                            pilaP.push("SUMI eax, " + v2);
                        case "*":
                            pilaP.push("MULTI eax, " + v2);
                    }
                } catch (Exception e) {
                    switch (pop.Nombre) {
                        case "-":
                            pilaP.push("RESTR eax, " + v2);
                        case "+":
                            pilaP.push("SUMR eax, " + v2);
                        case "*":
                            pilaP.push("MULTR eax, " + v2);
                    }
                }
            } else {
                break;
            }
        }
        v1 = pop.Nombre;
        pop = pila.pop();
        pilaP.push("MOV " + pop.Nombre + ", R16");
        return pila;
    }
    
    public void imprimir(){
        System.out.println("\n");
        System.out.println("\033[34mCodigo intermedio");
        while(!pilaP.isEmpty()){
            pilaP2.push(pilaP.pop());
        }
        while(!pilaP2.isEmpty()){
            mov.add(pilaP2.pop());
        }
        for (int i = 0; i < mov.size(); i++) {
            System.out.println(mov.getDato(i));
        }
    }
}

class Valor {

    String Nombre;
    String valor;

    public Valor(String Nombre, String valor) {
        this.Nombre = Nombre;
        this.valor = valor;
    }

    public String getNombre() {
        return Nombre;
    }

    public String getValor() {
        return valor;
    }
}
