package An_Lexico_Sintactico;

import EstructuraLista.ListaDinamica;
import EstructuraPila.PilaDinamica;

public class An_Sintactico {

    ListaDinamica<String> lista;
    PilaDinamica<String> pila = new PilaDinamica<>();
    An_Lexico lexico = new An_Lexico();
    String x;
    boolean existe, error = false;
    int row, col, valor, production;
    String ladoDer;
    int matriz[][]
            = {{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 6, 0, 0, 6, 0, 6, 6, 0, 0, 0},
            {0, 0, 0, 0, 0, 8, 0, 8, 0, 0, 7, 7, 7},
            {0, 0, 0, 10, 0, 0, 9, 0, 11, 12, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 13, 14, 15}};
    
    public An_Sintactico(){
        p_LlDriver();
        System.out.println("Programa paso a paso");
    }

    public void p_LlDriver() {
        pila.push("inicio");
        System.out.println("Se agrego a la pila el simbolo inicial");
        String a = lexico.getA();
        System.out.println("valor de a > " + a);
        while (!pila.isEmpty() && error == false) {
            x = pila.tope();
            System.out.println("tope de la plia (valor de x) > " + x);
            if (isNoTerminal(x)) {
                production = predict(x, a);
                if (production != 0) {
                    replaceX(production);
                } else {
                    System.out.println("\033[31mSe detecto un error sintactico " + a);
                    error = true;
                }
            } else {
                System.out.println("Se comparan x > " + x + " y a > " + a);
                if (x.equals(a)) {
                    pila.pop();
                    System.out.println("Se realizo un Pop en la pila");
                    a = lexico.getA();
                    System.out.println("Se obtuvo una nueva a > " + a);
                } else {
                    System.out.println("\033[31mSe detecto un error sintactico " + a);
                    error = true;
                }
            }
        }
        if (pila.isEmpty() && error == false) {
            System.out.println("No se detectaron errores sintacticos");
        }
    }
    
    private boolean isNoTerminal(String x) {
        existe = false;
        for (int i = 0; i < GeneradorEstructuras.nTerminal.size(); i++) {
            if (GeneradorEstructuras.nTerminal.getDato(i).equals(x)) {
                existe = true;
            }
        }
        return existe;
    }

    public int predict(String x, String a) {
        row = 0;
        col = 0;
        valor = 0;
        for (int i = 0; i < GeneradorEstructuras.nTerminal.size(); i++) {
            if (GeneradorEstructuras.nTerminal.getDato(i).equals(x)) {
                row = i;
            }
        }
        for (int i = 0; i < GeneradorEstructuras.terminal.size(); i++) {
            if (GeneradorEstructuras.terminal.getDato(i).equals(a)) {
                col = i;
            }
        }
        valor = matriz[row][col];
        return valor;
    }

    private void replaceX(int n) {
        System.out.println("\tSe realizara un reemplazo del simbolo");
        pila.pop();
        System.out.println("se realizo un Pop");
        ladoDer = GeneradorEstructuras.derecha.getDato(n-1);
        lista = new ListaDinamica<>();
        if (!ladoDer.equals("")) {
            fragmentar(ladoDer);
            for (int i = lista.size()-1; i >= 0; i--) {
                pila.push(lista.getDato(i));
                System.out.println("Se realizo un push a la pila > " + lista.getDato(i));
            }
        }
        System.out.println("\tFinalizo el reemplazo");
    }

    private void fragmentar(String txt) {
        int inicio = 0;
        int fin;
        int ascii;

        for (fin = 0; fin < txt.length(); fin++) {
            ascii = txt.charAt(fin);
            if (ascii == 9 || ascii == 32) {
                if (inicio != fin) {
                    this.lista.add(txt.substring(inicio, fin));
                    inicio = fin + 1;
                } else {
                    inicio++;
                }
            }
        }
        if (inicio != fin) {
            this.lista.add(txt.substring(inicio, fin));
        }
    }
}
