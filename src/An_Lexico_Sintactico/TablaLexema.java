package An_Lexico_Sintactico;

import EstructuraLista.ListaDinamica;

public class TablaLexema {

    static ListaDinamica<Renglon> lista = new ListaDinamica<>();
    static ListaDinamica<Renglon> mala = new ListaDinamica<>();
    int index, linea;

    public TablaLexema() {
    }

    public void aggregarLexema(String lexema, String token, String tipo, int valor, int linea) {
        this.linea = linea;
        index = 0;
        int contador = 0;
        boolean existe = false;
        for (int i = 0; i < lista.size(); i++) {
            if (lista.getDato(i).getLexema().equals(lexema)) {
                existe = true;
                contador = mala.getDato(i).getRepeticion() + 1;
                index = i;
            }
        }
        if (existe) {
            lista.modIndex(index, new Renglon(lexema, token, tipo, valor, (lista.getDato(index).getRepeticion() + 1), lista.getDato(index).getLinea() + ", " + linea));
            mala.add(new Renglon(lexema, token, tipo, valor, contador, "" + linea));
        } else {
            lista.add(new Renglon(lexema, token, tipo, valor, 1, "" + linea));
            mala.add(new Renglon(lexema, token, tipo, valor, 1, "" + linea));
        }
    }

    public void imprimirTabla() {
        System.out.println("\033[34mTabla de Tokens");
        System.out.println("\033[34mLexema\t\t\t||Token\t\t\t\t||Tipo\t\t\t\t||Valor\t\t\t||Repeticiones\t\t||");
        for (int i = 0; i < lista.size(); i++) {
            imprimir(lista.getDato(i).getLexema());
            imprimir(lista.getDato(i).getToken());
            imprimir(lista.getDato(i).getTipo());
            System.out.print(lista.getDato(i).getValor() + "\t\t||\t");
            System.out.print(lista.getDato(i).getRepeticion() + "\t\t||\t");
            System.out.println("");
        }
        System.out.println("");
    }

    private void imprimir(String str) {
        if (str.length() > 7) {
            if (str.length() < 16) {
                System.out.print(str + "\t\t||\t");
            } else {
                System.out.print(str + "\t||\t");
            }
        } else {
            System.out.print(str + "\t\t\t||\t");
        }
    }

    public void llamaTablaSimbolos() {
        new TablaSimbolos(lista);
    }
}

class Renglon {

    private String lexema;
    private String token;
    private String tipo;
    private int valor;
    private int rep;
    private String linea;

    public Renglon(String lexema, String token, String tipo, int valor, int rep, String linea) {
        this.lexema = lexema;
        this.token = token;
        this.tipo = tipo;
        this.valor = valor;
        this.rep = rep;
        this.linea = linea;
    }

    public String getLexema() {
        return this.lexema;
    }

    public String getToken() {
        return this.token;
    }

    public String getTipo() {
        return this.tipo;
    }

    public int getValor() {
        return this.valor;
    }

    public int getRepeticion() {
        return this.rep;
    }

    public String getLinea() {
        return linea;
    }
}