package An_Lexico_Sintactico;

import EstructuraLista.ListaDinamica;
import EstructuraPila.PilaDinamica;

public class ArbolSintacticoAbstracto {

    static ListaDinamica<Renglon> listaTokens;
    static ListaDinamica<RenglonSimbolos> listaSim;
    static ListaDinamica<RenglonSimbolos> listaArbolSA = new ListaDinamica<>();
    static ListaDinamica<String> prefija = new ListaDinamica<>();
    static PilaDinamica<RenglonSimbolos> pila = new PilaDinamica<>();
    static PilaDinamica<String> pilaPrefija = new PilaDinamica<>();
    static PilaDinamica<Valor> pilaPrefija2 = new PilaDinamica<>();
    CodigoP codigo = new CodigoP();
    RenglonSimbolos pop;
    String tipo1, tipo2, exp, signo, str;
    StringBuilder strPrefija;

    ArbolSintacticoAbstracto(ListaDinamica<Renglon> listaTokens) {
        this.listaTokens = listaTokens;

        for (int i = 0; i < listaTokens.size(); i++) {
            if (listaTokens.getDato(i).getLexema().equals(":=")) {
                asignacion(i - 1);
            }
        }
        imprimirTabla();
        codigo.imprimir();
    }

    private int asignacion(int index) {
        strPrefija = new StringBuilder();
        for (int i = index; i < listaTokens.size(); i++) {
            if (!listaTokens.getDato(i).getLexema().equals(";")) {
                if (!listaTokens.getDato(i).getLexema().equals(")")) {
                    pila.push(new RenglonSimbolos(listaTokens.getDato(i).getLexema(), listaTokens.getDato(i).getTipo(), listaTokens.getDato(i).getLexema(),
                            listaTokens.getDato(i).getRepeticion(), listaTokens.getDato(i).getLinea(), listaTokens.getDato(i).getLexema()));
                } else {
                    expression();
                }
            } else {
                expression();
                pop = pila.pop();
                tipo1 = pop.getTipo();
                pop = pila.pop();
                try {
                    Integer.parseInt(pop.getvAtributo());
                    listaArbolSA.add(new RenglonSimbolos(pop.getNombre(), pop.getTipo(),
                            pop.getvUnico(), pop.getRepeticion(), pop.getLinea(),
                            pop.getvAtributo()));
                } catch (Exception e) {
                    try {
                        Float.parseFloat(pop.getvAtributo());
                        listaArbolSA.add(new RenglonSimbolos(pop.getNombre(), pop.getTipo(),
                                pop.getvUnico(), pop.getRepeticion(), pop.getLinea(),
                                pop.getvAtributo()));
                    } catch (Exception e2) {
                        listaArbolSA.add(new RenglonSimbolos(pop.getNombre(), pop.getTipo(),
                                pop.getvUnico(), pop.getRepeticion(), pop.getLinea(), "0"));
                    }
                }
                pilaPrefija.push(pop.getNombre());
                index = i;
                break;
            }
        }
        while (!pilaPrefija.isEmpty()) {
            str = pilaPrefija.pop();
            strPrefija.append(str).append(" ");
            try {
                Integer.parseInt(str);
                pilaPrefija2.push(new Valor(str, str));
            } catch (Exception e) {
                try {
                    Float.parseFloat(str);
                    pilaPrefija2.push(new Valor(str, str));
                } catch (Exception e2) {
                    pilaPrefija2.push(new Valor(str, "0"));
                }
            }
        }
        prefija.add(strPrefija.toString());
        for (int i = 0; i < listaTokens.size(); i++) {
            if (listaTokens.getDato(i).getLexema().equals(pop.getNombre())) {
                listaTokens.modIndex(i, (new Renglon(listaTokens.getDato(i).getLexema(), listaTokens.getDato(i).getToken(),
                        tipo1, listaTokens.getDato(i).getValor(), listaTokens.getDato(i).getRepeticion(),
                        pop.getvAtributo())));
            }
        }
        pilaPrefija2 = codigo.codigo(pilaPrefija2);
        return index;
    }

    private void expression() {
        tipo1 = "";
        tipo2 = "";
        exp = "";
        signo = "";
        pop = pila.pop();
        try {
            Integer.parseInt(pop.getvAtributo());
            listaArbolSA.add(new RenglonSimbolos(pop.getNombre(), pop.getTipo(),
                    pop.getvUnico(), pop.getRepeticion(), pop.getLinea(),
                    pop.getvAtributo()));
        } catch (Exception e) {
            try {
                Float.parseFloat(pop.getvAtributo());
                listaArbolSA.add(new RenglonSimbolos(pop.getNombre(), pop.getTipo(),
                        pop.getvUnico(), pop.getRepeticion(), pop.getLinea(),
                        pop.getvAtributo()));
            } catch (Exception e2) {
                listaArbolSA.add(new RenglonSimbolos(pop.getNombre(), pop.getTipo(),
                        pop.getvUnico(), pop.getRepeticion(), pop.getLinea(), "0"));
            }
        }

        tipo1 = pop.getTipo();
        if (!pop.getNombre().equals("expression")) {
            pilaPrefija.push(pop.getNombre());
        }
        signo = pila.pop().getNombre();
        pop = pila.pop();
        try {
            Integer.parseInt(pop.getvAtributo());
            listaArbolSA.add(new RenglonSimbolos(pop.getNombre(), pop.getTipo(),
                    pop.getvUnico(), pop.getRepeticion(), pop.getLinea(),
                    pop.getvAtributo()));
        } catch (Exception e) {
            try {
                Float.parseFloat(pop.getvAtributo());
                listaArbolSA.add(new RenglonSimbolos(pop.getNombre(), pop.getTipo(),
                        pop.getvUnico(), pop.getRepeticion(), pop.getLinea(),
                        pop.getvAtributo()));
            } catch (Exception e2) {
                listaArbolSA.add(new RenglonSimbolos(pop.getNombre(), pop.getTipo(),
                        pop.getvUnico(), pop.getRepeticion(), pop.getLinea(), "0"));
            }
        }
        tipo2 = pop.getTipo();
        pilaPrefija.push(pop.getNombre());
        pilaPrefija.push(signo);
        signo = pila.pop().getNombre();
        if (signo.equals(":=")) {
            pilaPrefija.push(signo);
        }

        if (tipo1.equals("int") && tipo2.equals("int")) {
            exp = "int";
        } else if (tipo1.equals("int") && tipo2.equals("real")) {
            exp = "real";
        } else if (tipo1.equals("real") && tipo2.equals("int")) {
            exp = "real";
        } else if (tipo1.equals("real") && tipo2.equals("real")) {
            exp = "real";
        } else {
            exp = "error";
        }
        pila.push(new RenglonSimbolos("expression", exp, "null", 0, "null", "0"));
    }

    public void imprimirTabla() {
        System.out.println("\033[34mArbol Sintactico Abstracto");
        System.out.println("\033[34mNombre\t\t\t||Tipo\t\t\t\t||Valor identificacion (Unico)\t||Repeticiones\t\t||Linea\t\t\t||Valor del atributo\t\t||");
        for (int i = 0; i < listaArbolSA.size(); i++) {
            imprimir(listaArbolSA.getDato(i).getNombre());
            imprimir(listaArbolSA.getDato(i).getTipo());
            imprimir(listaArbolSA.getDato(i).getvUnico());
            System.out.print(listaArbolSA.getDato(i).getRepeticion() + "\t\t||\t");
            System.out.print(listaArbolSA.getDato(i).getLinea() + "\t\t||\t");
            imprimir(listaArbolSA.getDato(i).getvAtributo());
            System.out.println("");
        }
        System.out.println("");
        System.out.println("\033[34mNotacion Prefija");
        for (int i = 0; i < prefija.size(); i++) {
            System.out.println(prefija.getDato(i));
        }
    }

    private void imprimir(String str) {
        if (str.length() > 7) {
            if (str.length() < 16) {
                System.out.print(str + "\t\t||\t");
            } else {
                System.out.print(str + "\t||\t");
            }
        } else {
            System.out.print(str + "\t\t\t||\t");
        }
    }
}
