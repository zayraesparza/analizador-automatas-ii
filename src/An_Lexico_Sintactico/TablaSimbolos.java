package An_Lexico_Sintactico;

import EstructuraLista.ListaDinamica;


public class TablaSimbolos {

    static ListaDinamica<Renglon> listaTokens;
    static ListaDinamica<RenglonSimbolos> listaSim = new ListaDinamica<>();
    int vIdenti = 500;
    
    TablaSimbolos(ListaDinamica<Renglon> lista) {
        this.listaTokens = lista;
        aggregarSimbolo();
    }
    
    public void aggregarSimbolo() {
        
        for (int i = 0; i < listaTokens.size(); i++) {
            if (listaTokens.getDato(i).getToken().equals("Identificador")) {
                listaSim.add(new RenglonSimbolos(listaTokens.getDato(i).getLexema(), listaTokens.getDato(i).getTipo(), "" + vIdenti++, 
                        listaTokens.getDato(i).getRepeticion(), listaTokens.getDato(i).getLinea(), "" + 0));
            } else if (listaTokens.getDato(i).getToken().equals("realliteral") || listaTokens.getDato(i).getTipo().equals("intliteral")) {
                listaSim.add(new RenglonSimbolos(listaTokens.getDato(i).getLexema(), listaTokens.getDato(i).getTipo(), listaTokens.getDato(i).getLexema(), 
                        listaTokens.getDato(i).getRepeticion(), listaTokens.getDato(i).getLinea(), listaTokens.getDato(i).getLexema()));
            }
        }
//        imprimirTabla();
        llamaTablaArbolSintactico();
    }
    
    public void llamaTablaArbolSintactico() {
        new ArbolSintacticoAbstracto(TablaLexema.mala);
    }

    public void imprimirTabla() {
        System.out.println("\033[34mTabla de Simbolos");
        System.out.println("\033[34mNombre\t\t\t||Tipo\t\t\t\t||Valor de identificacion (Unico)\t||Repeticiones\t\t||Linea\t\t\t||Valor del atributo\t||");
        for (int i = 0; i < listaSim.size(); i++) {
            imprimir(listaSim.getDato(i).getNombre());
            imprimir(listaSim.getDato(i).getTipo());
            imprimir(listaSim.getDato(i).getvUnico());
//            System.out.print(listaSim.getDato(i).getvUnico()+ "\t\t\t\t||\t");
            System.out.print(listaSim.getDato(i).getRepeticion() + "\t\t||\t");
            System.out.print(listaSim.getDato(i).getLinea()+ "\t\t||\t");
            System.out.print(listaSim.getDato(i).getvAtributo()+ "\t||\t");
            System.out.println("");
        }
    }

    private void imprimir(String str) {
        if (str.length() > 7) {
            if (str.length() < 16) {
                System.out.print(str + "\t||\t");
            } else {
                System.out.print(str + "\t||\t");
            }
        } else {
            System.out.print(str + "\t\t||\t");
        }
    }
    
}

class RenglonSimbolos {

    private String nombre;
    private String tipo;
    private String vUnico;
    private int rep;
    private String linea;
    private String vAtributo;

    public RenglonSimbolos(String nombre, String tipo, String vUnico, int rep, String linea, String vAtributo) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.vUnico= vUnico;
        this.rep = rep;
        this.linea = linea;
        this.vAtributo = vAtributo;
    }

    public String getNombre() {
        return nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public String getvUnico() {
        return vUnico;
    }

    public int getRepeticion() {
        return rep;
    }

    public String getLinea() {
        return linea;
    }

    public String getvAtributo() {
        return vAtributo;
    }   
}
