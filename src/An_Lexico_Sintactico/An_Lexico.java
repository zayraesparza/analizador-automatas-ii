package An_Lexico_Sintactico;

import java.io.*;

public class An_Lexico {

    TablaLexema lexemas = new TablaLexema();
    String txt = "";
    String[] reservadas = {"programa", "begin", "end"};
    String[] caracteres = {";", "+", "-", "*", ":=", "(", ")"};
    int inicio = 0;
    int fin = 0;
    int con = 0;
    int linea = 0;
    boolean existe;

    public An_Lexico() {
        leer_Arch("programa.txt");
        System.out.println("\n\t\033[34mPrograma capturado del archivo de texto");
        System.out.println("\n" + txt);
    }

    private void leer_Arch(String file) {
        try {
            BufferedReader bf = new BufferedReader(new FileReader(file));
            StringBuilder temp = new StringBuilder();
            String read;
            while ((read = bf.readLine()) != null) {
                temp.append(read).append("\n");
            }
            txt = temp.toString();
        } catch (IOException e) {
            System.err.println("\033[31mOcurrio un error al leer el archivo" + e);
        }
    }
    
    public String getA(){
        String token = "";
        token = fragmentar(txt);
        return token;
    }

    public String fragmentar(String txt) {
        String retorna = "";
        int ascii;
        boolean detener = false;

        while(detener == false && fin < txt.length()){
            ascii = txt.charAt(fin);
            if (ascii == 9 || ascii == 32) {
                if (inicio != fin) {
                    retorna = analizar(txt.substring(inicio, fin));
                    detener = true;
                    inicio = fin + 1;
                } else {
                    inicio++;
                }
            } else if (ascii == 10) {
                if (inicio != fin) {
                    retorna = analizar(txt.substring(inicio, fin));
                    detener = true;
                    inicio = fin + 1;
                    linea++;
                } else {
                    inicio++;
                    linea++;
                }
            } else if (ascii == 59 || ascii == 40
                    || ascii == 41 || ascii == 42 || ascii == 43 || ascii == 45) {
                if (inicio == fin) {
                    retorna = analizar(txt.substring(inicio, fin + 1));
                    detener = true;
                    inicio++;
                } else {
                    retorna = analizar(txt.substring(inicio, fin));
                    detener = true;
                    inicio = fin;
                    fin--;
                }
            } else if (ascii == 58 && txt.charAt(++fin) == 61) {
                retorna = analizar(txt.substring(inicio, fin + 1));
                detener = true;
                inicio = fin + 1;
            }
            fin++;
        }
        if (inicio != fin) {
            retorna = analizar(txt.substring(inicio, fin));
        }
        return retorna;
    }

    private String analizar(String token) {
        String retorna = "";
        int estado;
        boolean sigue;
        con = 0;
        estado = 0;
        sigue = true;
        while (sigue) {
            switch (estado) {
                case 0:
                    if ('1' <= token.charAt(con) && token.charAt(con) <= '9') {
                        estado = 1;
                        con++;
                    } else if (token.charAt(con) == '0') {
                        estado = 2;
                        con++;
                    } else if (isAlphaM(token, con)) {
                        estado = 5;
                        con++;
                    } else if (isReservada(token)) {
                        estado = 8;
                    } else if (isCaracterSpecial(token)) {
                        estado = 9;
                    } else {
                        estado = 12;
                    }
                    break;
                case 1:
                    if (con < token.length()) {
                        if ('0' <= token.charAt(con) && token.charAt(con) <= '9') {
                            estado = 1;
                            con++;
                        } else if (token.charAt(con) == '.') {
                            estado = 3;
                            con++;
                        } else {
                            estado = 10;
                        }
                    } else {
                        retorna = "intliteral";
                        lexemas.aggregarLexema(token, retorna, "int", 301, linea);
                        sigue = false;
                    }
                    break;
                case 2:
                    if (con < token.length()) {
                        if (token.charAt(con) == '.') {
                            estado = 3;
                            con++;
                        } else {
                            estado = 10;
                        }
                    } else {
                        retorna = "intliteral";
                        lexemas.aggregarLexema(token, retorna, "int", 301, linea);
                        sigue = false;
                    }
                    break;
                case 3:
                    if (con < token.length()) {
                        if ('0' <= token.charAt(con) && token.charAt(con) <= '9') {
                            estado = 4;
                            con++;
                        } else {
                            estado = 10;
                        }
                    } else {
                        estado = 10;
                    }
                    break;
                case 4:
                    if (con < token.length()) {
                        if ('0' <= token.charAt(con) && token.charAt(con) <= '9') {
                            estado = 4;
                            con++;
                        } else {
                            estado = 10;
                        }
                    } else {
                        retorna = "realliteral";
                        lexemas.aggregarLexema(token, retorna, "real", 302, linea);
                        sigue = false;
                    }
                    break;
                case 5:
                    if (con < token.length()) {
                        if (isAlpha(token, con) || '0' <= token.charAt(con) && token.charAt(con) <= '9') {
                            estado = 5;
                            con++;
                        } else if (token.charAt(con) == '_') {
                            estado = 6;
                            con++;
                        } else {
                            estado = 11;
                        }
                    } else {
                        retorna = "id";
                        lexemas.aggregarLexema(token, "Identificador", "int", 303, linea);
                        sigue = false;
                    }
                    break;
                case 6:
                    if (con < token.length()) {
                        if (isAlpha(token, con) || '0' <= token.charAt(con) && token.charAt(con) <= '9') {
                            estado = 5;
                            con++;
                        }
                    } else {
                        estado = 11;
                    }
                    break;
                case 7:
                    if (con < token.length()) {
                        if (isAlpha(token, con) || '0' <= token.charAt(con) && token.charAt(con) <= '9') {
                            estado = 7;
                            con++;
                        } else {
                            estado = 11;
                        }
                    } else {
                        retorna = "id";
                        lexemas.aggregarLexema(token, "Identificador", "int", 303, linea);
                        sigue = false;
                    }
                    break;
                case 8:
                    retorna = token;
                    lexemas.aggregarLexema(token, "palabra reservada", token, (token.equals("programa")?304:(token.equals("begin")?305:306)), linea);
                    sigue = false;
                    break;
                case 9:
                    retorna = token;
                    lexemas.aggregarLexema(token, "palabra reservada", "simbolo del sistema", token.charAt(0), linea);
                    sigue = false;
                    break;
                case 10:
                    retorna = token;
                    sigue = false;
                    break;
                case 11:
                    retorna = token;
                    sigue = false;
                    break;
                case 12:
                    retorna = token;
                    sigue = false;
                    break;
            }
        }
        return retorna;
    }

    private boolean isReservada(String token) {
        existe = false;
        for (String s : reservadas) {
            if (token.equals(s)) {
                existe = true;
            }
        }
        return existe;
    }

    private boolean isCaracterSpecial(String token) {
        existe = false;
        for (String s : caracteres) {
            if (token.equals(s)) {
                existe = true;
            }
        }
        return existe;
    }

    private static boolean isAlpha(String s, int n) {
        int ascii = s.charAt(n);
        return 97 <= ascii && ascii <= 122;
    }

    private static boolean isAlphaM(String s, int n) {
        int ascii = s.charAt(n);
        return 65 <= ascii && ascii <= 90;
    }
}
