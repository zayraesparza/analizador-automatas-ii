package An_Lexico_Sintactico;

import EstructuraLista.ListaDinamica;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class GeneradorEstructuras {

    static String txt = "";
    static ListaDinamica<String> derecha = new ListaDinamica<>();
    static ListaDinamica<String> nTerminal = new ListaDinamica<>();
    static ListaDinamica<String> terminal = new ListaDinamica<>();

    public GeneradorEstructuras() {

        leer_Arch("gramatica.txt");
        System.out.println("\n\t \033[34mGramatica original");
        System.out.println(txt);
        System.out.println("\n\t\033[34mLado derecho de produccion");
        for (int i = 1; i < derecha.size(); i++) {
            System.out.println(derecha.getDato(i));
        }
        System.out.println("\n\t\033[34mNo terminales");
        for (int i = 1; i < nTerminal.size(); i++) {
            System.out.println(nTerminal.getDato(i));
        }
        System.out.println("\n\t\033[34mTerminales");
        for (int i = 1; i < terminal.size(); i++) {
            System.out.println(terminal.getDato(i));
        }
    }

    private static void leer_Arch(String file) {
        try {
            BufferedReader bf = new BufferedReader(new FileReader(file));
            StringBuilder temp = new StringBuilder();
            String read;
            while ((read = bf.readLine()) != null) {
                noTerminales(read);
            }
            bf = new BufferedReader(new FileReader(file));
            while ((read = bf.readLine()) != null) {
                temp.append(read).append("\n");
                clasifica(read);
            }
            txt = temp.toString();
        } catch (IOException e) {
            System.err.println("\033[31mOcurrio un error al leer el archivo" + e);
        }
    }

    public static void noTerminales(String txt) {
        int inicio = 0;
        int fin;
        int ascii;
        boolean existe = false;

        for (fin = 0; fin < txt.length(); fin++) {
            ascii = txt.charAt(fin);
            if (ascii == 9 || ascii == 32 || ascii == 45) {
                for (int i = 0; i < nTerminal.size(); i++) {
                    if (txt.substring(inicio, fin).equals(nTerminal.getDato(i))) {
                        existe = true;
                    }
                }
                if (!existe) {
                    nTerminal.add(txt.substring(inicio, fin));
                    inicio = fin = txt.length();
                }
            }
        }
    }

    public static void clasifica(String txt) {
        int inicio = 0;
        int fin;
        int ascii;
        boolean existe = false;

        for (fin = 0; fin < txt.length(); fin++) {
            ascii = txt.charAt(fin);
            if (ascii == 62) {
                fin++;
                inicio = fin;
                try {
                    derecha.add(txt.substring(inicio + 1, txt.length()));
                } catch (IndexOutOfBoundsException e) {
                    derecha.add("");
                }
                for (; fin < txt.length(); fin++) {
                    ascii = txt.charAt(fin);
                    if (ascii == 9 || ascii == 32) {
                        if (inicio != fin) {

                            for (int i = 0; i < nTerminal.size(); i++) {
                                if (txt.substring(inicio, fin).equals(nTerminal.getDato(i)) || txt.substring(inicio, fin).equals(terminal.getDato(i))) {
                                    existe = true;
                                }
                            }
                            if (!existe) {
                                if (txt.charAt(inicio) == 32) {
                                    terminal.add(txt.substring(inicio + 1, fin));
                                } else {
                                    terminal.add(txt.substring(inicio, fin));
                                }

                            }
                            inicio = fin + 1;
                        } else {
                            inicio++;
                        }
                    }
                }
                if (inicio != fin) {
                    existe = false;
                    for (int i = 0; i < nTerminal.size(); i++) {
                        if (txt.substring(inicio, fin).equals(nTerminal.getDato(i)) || txt.substring(inicio, fin).equals(terminal.getDato(i))) {
                            existe = true;
                        }
                    }
                    if (!existe) {
                        terminal.add(txt.substring(inicio, fin));
                    }
                }
            }
        }
    }
}
